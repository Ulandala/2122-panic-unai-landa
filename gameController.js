import {getAllHeroes} from './services/gameService.js'

const heroesData = async() => {
    try {
        const result = await getAllHeroes()
        console.log(result);
        return result
    }   catch (error) {
        console.log(error)
    }
}

export {heroesData}
