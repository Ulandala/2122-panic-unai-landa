import axios from 'axios'


const getAllHeroes = async() => {
    return axios.get('https://akabab.github.io/superhero-api/api')
    .then(function(response) {
        return response.data
    })
    .catch(function (error) {
        return error
    })
}

export {getAllHeroes}